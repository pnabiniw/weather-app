import requests
from django.shortcuts import render, redirect, get_object_or_404
from .models import City
# from .forms import CityForm


def index(request):
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=imperial&appid=4a74672adf0c47d3bc4a1a3f01d0a702'
    if request.method == 'POST':
        c = request.POST.get('city')
        form = City(name=c)
        form.save()
        return redirect('index')
    else:
        cities = City.objects.all()
        weather_list = list()
        for city in cities:
            r = requests.get(url.format(city)).json()
            city_weather = {
                'id': city.id,
                'city': city.name,
                'temperature': r['main']['temp'],
                'description': r['weather'][0]['description'],
                'icon': r['weather'][0]['icon'],
            }
            weather_list.append(city_weather)

        context = {'weather_list': weather_list}
        return render(request, 'weather/weather.html', context)


def delete(request, id):
    get_object_or_404(City, pk=id).delete()
    return redirect('index')


